/* Kathryn Lovett
 * CIS 315, Spring 2016
 * Assignment 2: a program that finds the longest path on a directed
 * acyclic graph (DAG), and the number of distinct longest paths
 */

import java.util.ArrayList;
import java.util.Scanner;

public class LongestPath {
	
	public static void main(String[] args) {
		// Initialize variables to hold number of graphs, nodes, and
		// edges for each graph, as well as an ArrayList to hold the results
		int graphNum = 0;
		int nodes = 0;
		int edges = 0;
		ArrayList<Integer> results = new ArrayList<>();
		
		// Initialize scanner and scan in number of graphs
		Scanner sc = new Scanner(System.in);
		graphNum = sc.nextInt();
		
		// For each graph, grab all of its input and find the 
		// number of and length of longest path(s)
		for(int i = 0; i < graphNum; i++) {
			// Scan in the number of nodes and edges for a graph
			nodes = sc.nextInt();
			edges = sc.nextInt();
			// Scan in the edges from standard input and store in an
			// ArrayList of ArrayLists of Integers
			ArrayList<ArrayList<Integer>> graph1 = createGraph(nodes, edges, sc);
			
			// Add the graph number to the ArrayList results and then
			// find the longest path in the graph
			results.add(i+1);
			findLongest(graph1, nodes, results);
		}
		// Print the results of finding the longest path in each graph
		printResults(results);
		
		// Close the scanner
		sc.close();
	}
	
	public static ArrayList<ArrayList<Integer>> createGraph(int nodes, int edges, Scanner sc) {
		// Initialize ArrayList to store edges and weight information
		ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
		
		// Scan in the next start vertex and initialize the end
		// vertex, the weight, and a variable to track the last start
		// vertex
		int next = sc.nextInt();
		int end = 0;
		int weight = 0;
		int last = 0;
		// Initialize ArrayList to hold edge information (start, end, weight)
		ArrayList<Integer> temp = new ArrayList<>();
		
		for(int i = 0; i < edges; i++) {
			// For each edge, scan in the end and the weight,
			// add them to the ArrayList temp, and if the next
			// start vertex is different than the last, add temp
			// to graph and make a new ArrayList
			end = sc.nextInt();
			weight = sc.nextInt();
			temp.add(end);
			temp.add(weight);

			if(i < (edges-1)) {
				last = next;
				next = sc.nextInt();
				if(last != next) {
					graph.add(temp);
					temp = new ArrayList<>();
				}
			}
			else {
				graph.add(temp);
			}
		}
		return graph;
	}
	
	public static void findLongest(ArrayList<ArrayList<Integer>> graph, int nodes, ArrayList<Integer> results) {
		// Initialize new array of integers to hold the longest path to each vertex
		// Make longest path to vertex 1 equal 0
		int[] LP = new int[nodes+1];
		LP[1] = 0;
		
		// Initialize new array of integers to hold the number of longest paths to 
		// each vertex, and make the number of longest paths to vertex 1 equal 1
		int[] numLP = new int[nodes+1];
		numLP[1] = 1;
		
		// Initialize integer to hold minimum value, and set the longest
		// path to each vertex after 1 to that integer
		int inf = Integer.MIN_VALUE;
		for(int i = 2; i < nodes; i++) {
			LP[i] = inf; 
		}
		
		// For each vertex in the graph, check its connecting edges
		for(int i = 0; i < graph.size(); i++) {
			// For each edge that vertex i+1 connects to, check its
			// longest path
			for(int j = 0; j < graph.get(i).size(); j+=2) {
				// Initialize current to the end vertex that i+1 connects to
				int current = graph.get(i).get(j);
				
				// If the current longest path equals the path from the 
				// vertex i+1 to this end vertex, add the number of 
				// longest paths of i+1 to those of the end vertex
				if(LP[current] == (LP[i+1]+graph.get(i).get(j+1))) {
					numLP[current] += numLP[i+1];
				}
				else if (LP[current] < (LP[i+1]+graph.get(i).get(j+1))){
					// If the current longest path is less than the one 
					// being looked at, make the current equal the new one and 
					// make the number of longest paths equal to those of vertex i+1
					numLP[current] = numLP[i+1];
					LP[current] = (LP[i+1]+graph.get(i).get(j+1));
				}
			}
		}
		// Add the length of the longest path and the number of unique
		// longest path(s) to the ArrayList results
		results.add(LP[nodes]);
		results.add(numLP[nodes]);
	}
	
	public static void printResults(ArrayList<Integer> results) {
		// Every grouping of three in the ArrayList results contains 
		// the results for a specific graph, therefore look at each 
		// grouping and print out its information
		for(int i = 0; i < results.size(); i += 3) {
			System.out.printf("graph number: %d \n", results.get(i));
			System.out.printf("longest path: %d \n", results.get(i+1));
			System.out.printf("number of longest paths: %d \n\n", results.get(i+2));
		}
	}

}
