/* Kathryn Lovett
 * CIS 315, Spring 2016
 * Assignment 0: A program that reads a series of pairs of ints and 
 * prints their sum and product.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Compute {

	public static void main(String[] args) {
		// Declaration of int variable that holds number of pairs
		int inputNum = 0;
		// Declaration of ArrayList to hold pairs of numbers
		ArrayList<Integer> numList = new ArrayList<Integer>();
		
		// Initialization of scanner
		Scanner sc = new Scanner(System.in);
		// Scans in first int and saves as inputNum
		inputNum = sc.nextInt();
		
		// Scans twice as many numbers as the inputNum, and stores
		// in ArrayList numList
		for(int i = 0; i < inputNum * 2; i++) {
			numList.add(sc.nextInt());
		}
		
		// Calls function that returns an ArrayList containing
		// the sums and products of every two numbers, saving them
		// in the ArrayList results
		ArrayList<Integer> results = calculateSumProduct(numList);
		
		// Prints out the resultant sums and products
		printResults(results);
		
		// Closes the scanner
		sc.close();
	}
	
	public static ArrayList<Integer> calculateSumProduct(ArrayList<Integer> numList) {
		// Declares ArrayList results to store sums and products
		ArrayList<Integer> results = new ArrayList<Integer>();
		
		// Loops through items in numList and, for every pair, 
		// saves the sum and product in the ArrayList results
		for(int k = 0; k < numList.size(); k++) {
			if((k % 2) == 0) {
				results.add(numList.get(k) + numList.get(k+1));
			}
			else {
				results.add(numList.get(k-1) * numList.get(k));
			}
		}
		
		// Returns the ArrayList results
		return results;
	}
	
	public static void printResults(ArrayList<Integer> results) {
		// Loops through results ArrayList and prints out the 
		// sum and product of each pair
		for(int j = 0; j < results.size(); j++) {
			System.out.print(results.get(j) + " ");
			if((j % 2) != 0 && j != 0) {
				System.out.println();
			}
		}
	}

}
