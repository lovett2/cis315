Kathryn Lovett
lovett2@uoregon.edu
To compile: javac *.java
To run memoized program: java prettyMemo
To run iterative version: java prettyIter
