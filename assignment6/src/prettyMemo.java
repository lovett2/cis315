import java.util.Scanner;

/* Kathryn Lovett
 * CIS 315, Spring 2016
 * Assignment 6: Pretty Printing, memoized version
 */
public class prettyMemo {
	// Penalty contains penalties for each possible combination of words on a single line
	// N holds the minimum penalty for word i to the end of the paragraph
	// BP holds the breakpoint that provides the minimum penalty for N
	// n is the number of words
	// linLength is the length of a line
	// words is an array of all of the words in the paragraph
	static int penalty[][];
	static int N[];
	static int BP[];
	static int n;
	static int lineLength;
	static String[] words;

	public static void main(String[] args) {
		// scan in the number of paragraphs, then for each paragraph, determine the arrangement
		// of words that will result in the lowest penalty and print out the paragraph in that way
		int paragraphNum = 0;
		String myString = "";
		Scanner sc = new Scanner(System.in);
		paragraphNum = Integer.parseInt(sc.nextLine());

		for(int i = 0; i < 2*paragraphNum; i++) {
			if(i % 2 == 0) {
				lineLength = Integer.parseInt(sc.nextLine());
			}
			else {
				myString = sc.nextLine();
				parseString(myString);
				for(int j = 0; j < n; j++) {
					for(int k = j; k < n; k++) {
						penalty[j][k] = p(j, k);
					}
				}
				print();
			}
		}
		System.out.println();
		sc.close();
	}
	
	public static int cube(int n) {
		// return n cubed
		return (n*n*n);
	}
	
	public static void parseString(String myString) {
		// parse the input string/paragraph, initialize arrays
		words = myString.split(" ");
		n = words.length;
		N = new int[n];
		BP = new int[n];
		penalty = new int[n][n];
		for(int i = 0; i < n; i++) {
			N[i] = -1;
			BP[i] = n;
		}
		
		for(int j = 0; j < n; j++) {
			for(int k = 0; k < n; k++) {
				penalty[j][k] = -1;
			}
		}
	}
	public static void print() {
		// print out paragraph based upon minimum penalty
		int current = 0;
		int i, end;
		System.out.println();
		System.out.println("penalty: " + memoN(0));
		while(current < n) {
			end = BP[current];
			for(i = current; i < end; i++) {
				System.out.print(words[i] + " ");
			}
			System.out.println();
			current = end;
		}
	}
	
	public static int p(int i, int j) {
		// determine the penalty for going from word i to word j on a single line
		// return Integer.MAX_VALUE if words i to j cannot fit on a single line
		int totalLength = 0;
		int wordSum = 0;
		int penalty = 0;
		
		totalLength = lineLength - j + i;
		
		for(int k = i; k <= j; k++) {
			wordSum += words[k].length();
		}
		
		totalLength -= wordSum;
		
		if(totalLength < 0) {
			penalty = Integer.MAX_VALUE;
		}
		else {
			penalty = cube(totalLength);
		}
		
		return penalty;
	}
	
	public static int memoN(int i) {
		if(i == n) {
			return 0;
		}

		if(N[i] == -1) {
			if(penalty[i][n-1] < Integer.MAX_VALUE) {
				N[i] = 0;
			}
			else {
				int j = i;
				N[i] = Integer.MAX_VALUE;
				while(j < n) {
					if(penalty[i][j] < Integer.MAX_VALUE && N[i] > (penalty[i][j] + memoN(j+1))) {
						N[i] = penalty[i][j] + N[j+1];
						BP[i] = j+1;
					}
					j++;
				}
			}
		}
		return N[i];
	}
}
